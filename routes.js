const Router = require('express').Router;
const router = new Router();

const event = require('./model/event/router');
const user = require('./model/user/router');

router.route('/').get((req, res) => {
  res.json({ message: 'Welcome to lunch-box API!' });
});

router.use('/api/events', event);
router.use('/api/users', user);

module.exports = router;
