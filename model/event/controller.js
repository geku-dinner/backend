const mongoose   = require('mongoose');

const Controller = require('../../lib/controller');
const eventFacade = require('./facade');

class EventController extends Controller {

  find(req, res, next) {
    return this.facade.find({
      $and: [
        { date: {$gte: req.query.from} },
        { date: {$lte: req.query.to} }
      ]
    })
      .then(collection => res.status(200).json(collection))
      .catch(err => next(err));
  }

  update(req, res, next) {

    req.params.id =  req.params.id || new mongoose.Types.ObjectId;

    this.facade.update({ _id: req.params.id }, req.body, {upsert: true, setDefaultsOnInsert: true})
      .then((results) => {
        if (results.n < 1) { return res.sendStatus(404); }
        if (results.nModified < 1) { return res.sendStatus(304); }
        res.sendStatus(204);
      })
      .catch(err => next(err));
  }

  setCook(req, res, next) {
    this.facade.update({ date: req.params.date }, { cook: req.params._user }, {new: true, upsert: true, setDefaultsOnInsert: true} )
      .then((results) => {
        // if (results.n < 1) { return res.sendStatus(404); }
        // if (results.nModified < 1) { return res.sendStatus(304); }
        res.status(200).json(results);
      })
      .catch(err => next(err));
  }

  removeCook(req, res, next) {
    this.facade.update({ date: req.params.date }, { cook: undefined }, {new: true, upsert: true, setDefaultsOnInsert: true} )
      .then( results => {
        //console.log(results);
        //if (results.n < 1) { return res.sendStatus(404); }
        //if (results.nModified < 1) { return res.sendStatus(304); }
        res.status(200).json(results);
      })
      .catch(err => next(err));
  }

  addParticipant(req, res, next) {
    this.facade.update({ date: req.params.date }, { $addToSet: { participants: req.params._user } }, {new: true, upsert: true, setDefaultsOnInsert: true} )
      .then( results => {
        //console.log(results);
        //if (results.n < 1) { return res.sendStatus(404); }
        //if (results.nModified < 1) { return res.sendStatus(304); }
        res.status(200).json(results);
      })
      .catch(err => next(err));
  }

  removeParticipant(req, res, next) {
    this.facade.update({ date: req.params.date }, { $pull: { participants: req.params._user } }, {new: true, upsert: true, setDefaultsOnInsert: true} )
      .then((results) => {
        //if (results.n < 1) { return res.sendStatus(404); }
        //if (results.nModified < 1) { return res.sendStatus(304); }
        res.status(200).json(results);
      })
      .catch(err => next(err));
  }
}

module.exports = new EventController(eventFacade);
