const Facade = require('../../lib/facade');
const eventSchema = require('./schema');

class EventFacade extends Facade {

  create(body) {
    const schema = new this.Schema(body);
    schema.save();
    return this.Schema.populate(schema, {path: 'cook'});
  }

  find(...args) {
    return this.Schema
      .find(...args)
      .populate('cook')
      .populate('participants')
      .exec();
  }

  update(...args) {
    return this.Schema
      .findOneAndUpdate(...args)
      .populate('cook')
      .populate('participants')
      .exec();
  }

  aggregate(...args) {
    return this.Schema
      .aggregate(...args)
      .exec();
  }

}

module.exports = new EventFacade(eventSchema);
