const controller = require('./controller');
const Router = require('express').Router;
const router = new Router();

router.route('/')
  .get((...args) => controller.find(...args))
  .post((...args) => controller.create(...args))
  .put((...args) => controller.update(...args));

router.route('/:id')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findById(...args))
  .delete((...args) => controller.remove(...args));

router.route('/:date/participants/:_user')
  .put((...args) => controller.addParticipant(...args))
  .delete((...args) => controller.removeParticipant(...args));

router.route('/:date/cook')
  .delete((...args) => controller.removeCook(...args));

router.route('/:date/cook/:_user')
  .put((...args) => controller.setCook(...args));

module.exports = router;
