const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const eventSchema = new Schema({
  date: { type: Date, required: true },
  time: Date,
  meal: String,
  cook: { type: Schema.Types.ObjectId, ref: 'User' },
  participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],

});


module.exports = mongoose.model('Event', eventSchema);
