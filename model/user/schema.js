const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Event = require('../event/facade');


const userSchema = new Schema({
  name: { type: String, required: true }
}, {
  toObject: { virtuals: true },
  toJSON: { virtuals: true }
});

userSchema.fill('credits').multi( function(docs, ids, callback) {
  this.db.model('Event')
    .aggregate([
        { $match: {cook: {$in: ids}} },
        { 
          $group: { 
            _id: '$cook',
            credits: { 
              $sum: { $size: '$participants' }
            }
          }
        }
    ], callback);
});

userSchema.fill('participations').value( function(callback) {
  this.db.model('Event')
    .count({participants: this._id})
    .exec(callback);
});


module.exports = mongoose.model('User', userSchema);
