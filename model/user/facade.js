const Facade = require('../../lib/facade');
const userSchema = require('./schema');

class UserFacade extends Facade {

  find(...args) {
    return this.Schema
      .find(...args)
      .select('name credits participations')
      .exec();
  }

}

module.exports = new UserFacade(userSchema);
